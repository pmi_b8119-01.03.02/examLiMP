examLiMP
=====================
###### ***задачи для экзамена по ЯиМПу***

##### Со мной вы сможете связаться:
1. [Telegram](https://tgmsg.ru/princepepper)
2. [Вконтакте](https://vk.com/princepepper)
3. [Instargam](https://www.instagram.com/prince_pepper_official/?hl=ru)
4. [Мой сайт](http://www.ppts.website/)(в разработке)

##### Полезные ссылки:
* [Dpaste](https://dpaste.de/) - предназначен для передачи кода
* [Pastebin](https://pastebin.com/) - предназначен для передачи кода, но менее удобен
* [Руководство по стилю написания кода на С](https://cs50.readthedocs.io/style/c/)
* [Алгоритмы и структуры данных](https://mathmachine.github.io/wiki/algorithms.html)
